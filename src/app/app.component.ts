import { Component, OnInit } from "@angular/core";
import { NgxGalleryOptions, NgxGalleryImage } from "ngx-gallery";

@Component({
  selector: "cms-gallery",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class CmsGallery implements OnInit {
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];

  ngOnInit(): void {
    this.galleryImages = [
      {
        small:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/1-small.jpeg",
        medium:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/1-medium.jpeg",
        big:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/1-big.jpeg"
      },
      {
        small:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/2-small.jpeg",
        medium:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/2-medium.jpeg",
        big:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/2-big.jpeg"
      },
      {
        small:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/3-small.jpeg",
        medium:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/3-medium.jpeg",
        big:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/3-big.jpeg"
      },
      {
        small:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/4-small.jpeg",
        medium:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/4-medium.jpeg",
        big:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/4-big.jpeg"
      },
      {
        small:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/5-small.jpeg",
        medium:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/5-medium.jpeg",
        big:
          "https://lukasz-galka.github.io/ngx-gallery-demo/assets/img/5-big.jpeg"
      }
    ];
  }
}
