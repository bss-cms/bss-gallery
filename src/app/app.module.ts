import { Injector, NgModule } from "@angular/core";
import { createCustomElement } from "@angular/elements";
import { BrowserModule } from "@angular/platform-browser";
import { CmsGallery } from "./app.component";
import { NgxGalleryModule } from "ngx-gallery";

@NgModule({
  declarations: [CmsGallery],
  imports: [BrowserModule, NgxGalleryModule],
  providers: [],
  entryComponents: [CmsGallery]
})
export class AppModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap() {
    const myElement = createCustomElement(CmsGallery, {
      injector: this.injector
    });
    customElements.define("cms-gallery", myElement);
  }
}
